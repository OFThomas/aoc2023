# each line find the first and last digit
# then sum all of these 2 digit numbers over each line 
import std/strutils
import std/tables

block part1:
  echo "Part1"
  var sum = 0
  for line in lines "input.txt":
    if line.isEmptyOrWhitespace:
      continue
    var digits = ""
    for i in 0..<line.len:
      if line[i].isDigit:
        digits.add line[i]
        break
    for i in countdown(line.len-1, 0):
      if line[i].isDigit:
        digits.add line[i]
        break
    # echo "Digits: ", digits
    sum += digits.parseInt
  echo sum

# part2
block part2:
  echo "Part2"
  const lookup = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

  var sum = 0
  for line in lines "input.txt":
    if line.isEmptyOrWhitespace:
      continue
    var digits = ""
    # fwd pass
    block FWDPASS:
      for i in 0..<line.len:
        if line[i].isDigit:
          digits.add line[i]
          break FWDPASS
        # check if part of word in lookup
        if line[i].isAlphaAscii:
          for key in lookup:
            if key.contains(line[i]):
              if line.len < i+key.len:
                continue
              if key == line[i..<i+key.len]:
                # echo "found key ", key
                digits.add $(lookup.find(key)+1)
                break FWDPASS

    block BCKWDPASS:
      for i in countdown(line.len-1, 0):
        if line[i].isDigit:
          digits.add line[i]
          break BCKWDPASS
        # check if part of word in lookup
        if line[i].isAlphaAscii:
          for key in lookup:
            if key.contains(line[i]):
              if i - key.len + 1 < 0:
                continue
              if key == line[i - key.len + 1..i]:
                # echo "found key ", key
                digits.add $(lookup.find(key)+1)
                break BCKWDPASS

    # echo "Digits: ", digits
    sum += digits.parseInt
  echo sum



